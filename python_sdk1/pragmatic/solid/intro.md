# SOLID


El objetivo de este artículo es que el lector aprenda aplicar los principios **SOLID** con el lenguaje Python. **SOLID** es un acrónimo creado por *Michael Feathers* para los principios publicados por *Robert C. Martin*, en su libro *Agile Software Development: Principles, Patterns, and Practices*.

Se trata de cinco principios de diseño orientado a objetos que nos ayudarán a crear mejor código, más estructurado, con clases de responsabilidad más definida y más desacopladas entre sí:

* **Single Responsibility**: Responsabilidad única.
* **Open/Closed**: Abierto/Cerrado.
* **Liskov substitution**: Sustitución de Liskov.
* **Interface segregation**: Segregación de interfaz.
* **Dependency Inversion**: Inversión de dependencia.


Es importante resaltar que se trata de principios, no de reglas. Una regla es de obligatorio cumplimiento, en cambio, los principios son recomendaciones que pueden ayudar a hacer las cosas mejor. Además, siempre puedes encontrar algún contexto en el que te los puedas saltar, lo importante es hacerlo de forma consciente.
